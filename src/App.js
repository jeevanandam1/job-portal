import Badge from "./components/Badge";
import Banner from "./components/Banner";
import Button from "./components/Button";
import Card from "./components/Card";
import TextTitle from "./components/TextTitle";
import SubTitle from "./components/SubTitle";

function App() {
	const subMain = "SP Infocity  Street,  Chennai,";

	const subText =
		"Snapchat is an American multimedia messaging app developed by Snap Inc., originally Snapchat Inc. One of the principal features of Snapchat is that pictures and messages are usually only available for a short time before they become inaccessible to their recipients.";

	const desc = [
		"Minimum 2+ years experience in Front End Development",
		"Strong knowledge of Android SDK, different versions of Android, and how to deal with different screen ",
		"Familiarity with cloud messaging APIs and push notifications",
		"Sound knowledge on RESTful and GraphQL APIs",
		"Proficient in Git and familiarity with continuous integration",
	];

	const list = ["HTML", "CSS", "JavaScript", "jQuery", "Python", "Go", "Ruby"];

	const tools = [
		"React",
		"Vue",
		"Bootstrap",
		"Codepen",
		"TailwindCSS",
		"VS code",
		"Version control",
	];

	return (
		<div>
			<Banner />
			<div className="px-4 mx-auto text-center md:text-left md:w-11/12 2xl:w-9/12">
				<TextTitle title={"Snapchat"} fontSize={true}>
					<SubTitle text={subMain} fontSize={true} />
				</TextTitle>
				<Card />
				<TextTitle title={"Overview"}>
					<SubTitle text={subText} />
				</TextTitle>
				<TextTitle title={"Job Description"}>
					{desc.map((text) => {
						return (
							<ul className="text-left list-disc">
								<li className="m-4 text-teal-300">
									<span className="text-black">{text}</span>
								</li>
							</ul>
						);
					})}
				</TextTitle>
				<TextTitle title={"Programming Languages"}>
					<Badge list={list} />
				</TextTitle>
				<TextTitle title={"Tools"} list={tools}>
					<Badge list={tools} />
				</TextTitle>
				<Button btnText={"Apply now"} />
			</div>
		</div>
	);
}

export default App;
