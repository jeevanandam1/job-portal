export default function Badge({ list }) {
	return (
		<div className="flex flex-wrap justify-center gap-4 md:justify-start">
			{list.map((item) => (
				<div className="py-3.5 px-5 bg-theme-blue-300 rounded-xl">
					<p className="text-sm font-normal text-theme-green-500">{item}</p>
				</div>
			))}
		</div>
	);
}
