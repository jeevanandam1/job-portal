export default function TextTitle({ title, children, fontSize }) {
	return (
		<div className="my-8">
			<p
				className={`my-4 font-bold text-black  ${
					fontSize ? "text-2xl" : "text-xl"
				}`}
			>
				{title}
			</p>
			{children}
		</div>
	);
}
