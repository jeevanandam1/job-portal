export default function Subtitle({ text, fontSize }) {
	return (
		<div>
			<p
				className={` font-normal text-black ${
					fontSize ? "text-sm" : "text-base"
				}`}
			>
				{text}
			</p>
		</div>
	);
}
