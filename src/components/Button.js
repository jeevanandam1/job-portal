export default function Button({ btnText }) {
	return (
		<div className="py-6 ">
			<button className="w-full p-4 text-sm font-bold text-white uppercase rounded-lg hover:bg-opacity-90 bg-theme-green-500">
				{btnText}
			</button>
		</div>
	);
}
