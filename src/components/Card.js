export default function Card() {
	const text = [
		{
			mainText: "Role",
			subText: "Frontend Developer",
		},
		{
			mainText: "Level",
			subText: "Midweight",
		},
		{
			mainText: "Contract",
			subText: "FullTime Employee",
		},
	];

	return (
		<div className="flex flex-wrap justify-center gap-6 md:justify-start">
			{text.map((value) => (
				<div className="flex flex-col items-center justify-center w-full h-24 p-3 rounded-lg md:items-start md:w-64 bg-theme-blue-300">
					<p className="mb-2 text-sm font-medium text-theme-gray-300">
						{value.mainText}
					</p>
					<p className="mt-2 text-base font-bold text-theme-gray-500">
						{value.subText}
					</p>
				</div>
			))}
		</div>
	);
}
