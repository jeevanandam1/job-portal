import BannerImage from "../assets/header.jpg";
import SnapChat from "../assets/snapchat.png";

export default function Banner() {
	return (
		<div className="relative">
			<div className="pb-5">
				<img
					src={BannerImage}
					alt="Banner"
					className="object-cover object-center w-full h-full md:h-96"
				/>
				<div className="absolute border-4 border-white bg-white rounded-full -translate-x-2/4 left-2/4 md:left-[7.5%] top-[85%]">
					<img
						src={SnapChat}
						className="w-10 h-10 md:w-20 md:h-20"
						alt="SnapChat"
					/>
				</div>
			</div>
		</div>
	);
}
