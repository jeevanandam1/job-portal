/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ["./src/**/*.{js,jsx,ts,tsx}"],
	theme: {
		extend: {
			colors: {
				"theme-gray-300": "#828282",
				"theme-gray-500": "#333333",
				"theme-blue-300": "#f3f7f9",
				"theme-green-500": "#5da5a4",
			},
		},
	},
	plugins: [],
};
